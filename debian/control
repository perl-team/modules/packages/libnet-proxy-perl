Source: libnet-proxy-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Michael Ablassmeier <abi@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libhttp-daemon-perl <!nocheck>,
                     libio-socket-ssl-perl <!nocheck>,
                     libscalar-list-utils-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     libwww-perl <!nocheck>,
                     perl
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libnet-proxy-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libnet-finger-perl.git
Homepage: https://metacpan.org/release/Net-Proxy
Rules-Requires-Root: no

Package: libnet-proxy-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libio-socket-ssl-perl,
         libscalar-list-utils-perl,
         libwww-perl
Conflicts: sslh
Description: framework for proxying network connections in many ways
 The Net::Proxy module is a framework for creating various kinds of network
 proxies in a very simple way.
 .
 A proxy is a program that transfer data across a network boundary between
 a client and a server. Net::Proxy introduces the concept of "connectors",
 which abstract the server part (connected to the client) and the client
 part (connected to the server) of the proxy.
 .
 This makes it very easy to implement specific techniques to cross a
 given network boundary, possibly by using a proxy on one side of the
 network fence, and a reverse-proxy on the other side of the fence.
 .
 This package also provides the sslh SSH/HTTPS service multiplexer
 written in perl.
